package survey

import (
	"fmt"
	"gitlab.com/eper.io/grotto/drawing"
	"net/http"
	"strings"
)

// This document is Licensed under Creative Commons CC0.
// To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights
// to this document to the public domain worldwide.
// This document is distributed without any warranty.
// You should have received a copy of the CC0 Public Domain Dedication along with this document.
// If not, see https://creativecommons.org/publicdomain/zero/1.0/legalcode.

func SetupSurvey() {
	http.HandleFunc("/index.html", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/survey.html", http.StatusTemporaryRedirect)
	})

	http.HandleFunc("/survey.html", func(w http.ResponseWriter, r *http.Request) {
		err := drawing.EnsureAPIKey(w, r)
		if err != nil {
			return
		}
		drawing.ServeRemoteForm(w, r, "survey")
	})

	http.HandleFunc("/survey.png", func(w http.ResponseWriter, r *http.Request) {
		session := drawing.GetSession(w, r)
		if session != nil {
			session.Mutex.Lock()
			declareForm(drawing.GetSession(w, r))
			drawing.ProcessInputs(w, r)
			session.Mutex.Unlock()
		}
	})
}

func declareForm(session *drawing.Session) {
	const Contact = 0

	const CheckMark1 = 1
	const CheckMark2 = 3
	const CheckMark3 = 5
	const CheckMark4 = 7
	const CheckMark5 = 9

	const Question1 = 2
	const Question2 = 4
	const Question3 = 6
	const Question4 = 8
	const Question5 = 10

	const Submit = 11

	if session.Form.Boxes == nil {
		drawing.DeclareForm(session, "./survey/res/grotto.png")

		drawing.SetImage(session, Contact, "./drawing/res/space.png", drawing.Content{Text: "", Lines: 1, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: 1})

		drawing.PutText(session, Question1, drawing.Content{Text: "Do you work from a home office?\n ", Lines: 2, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: -1})
		drawing.SetImage(session, CheckMark1, "./survey/res/unchecked.png", drawing.Content{Text: "", Lines: 1, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: 1})
		drawing.PutText(session, Question2, drawing.Content{Text: "Have you spent on holiday travel\nin the last three months?", Lines: 2, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: -1})
		drawing.SetImage(session, CheckMark2, "./survey/res/unchecked.png", drawing.Content{Text: "", Lines: 1, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: 1})
		drawing.PutText(session, Question3, drawing.Content{Text: "Will you spend more on holiday travel\nin the next three months?", Lines: 2, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: -1})
		drawing.SetImage(session, CheckMark3, "./survey/res/unchecked.png", drawing.Content{Text: "", Lines: 1, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: 1})
		drawing.PutText(session, Question4, drawing.Content{Text: "     Have you spent on education\nin the last three months?", Lines: 2, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: -1})
		drawing.SetImage(session, CheckMark4, "./survey/res/unchecked.png", drawing.Content{Text: "", Lines: 1, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: 1})
		drawing.PutText(session, Question5, drawing.Content{Text: "Will you spend more on education\nin the next three months?", Lines: 2, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: -1})
		drawing.SetImage(session, CheckMark5, "./survey/res/unchecked.png", drawing.Content{Text: "", Lines: 1, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: 1})

		drawing.PutText(session, Submit, drawing.Content{Text: "         Terms         ", Lines: 1, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: 0})

		UpdateDebuggingInformation(session, Question1, CheckMark1-Question1, "Initial. ")

		session.SignalClicked = func(session *drawing.Session, i int) {
			if i == Submit {
				if strings.HasPrefix(sessions[session.ApiKey], "In progress. ") {
					UpdateDebuggingInformation(session, Question1, CheckMark1-Question1, "Submitted. ")
					drawing.PutText(session, Submit, drawing.Content{Text: "      Thank you.       ", Lines: 1, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: 0})
					session.SignalPartialRedrawNeeded(session, Submit)
				} else {
					session.Redirect = fmt.Sprintf("/terms.txt")
					session.SignalClosed(session)
				}
				return
			}
			if i == CheckMark1 || i == CheckMark2 || i == CheckMark3 || i == CheckMark4 || i == CheckMark5 {
				checked := false
				if strings.HasSuffix(session.Text[i].BackgroundFile, "unchecked.png") {
					checked = true
				}
				fileName := "./survey/res/unchecked.png"
				if checked {
					fileName = "./survey/res/checked.png"
				}

				drawing.SetImage(session, i, fileName, drawing.Content{Text: "", Lines: 1, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: 1})
				UpdateDebuggingInformation(session, Question1, CheckMark1-Question1, "In progress. ")
				session.SignalPartialRedrawNeeded(session, i)
				drawing.PutText(session, Submit, drawing.Content{Text: "        Submit         ", Lines: 1, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: 0})
				session.SignalPartialRedrawNeeded(session, Submit)
			}
		}
		session.SignalClosed = func(session *drawing.Session) {
			session.SelectedBox = -1
		}
	}
}

func UpdateDebuggingInformation(session *drawing.Session, base int, shift int, status string) {
	log := status
	for i := 0; i < 10; i = i + 2 {
		checked := true
		img := session.Text[base+i+shift].BackgroundFile
		if strings.HasSuffix(img, "unchecked.png") {
			checked = false
		}
		log = log + fmt.Sprintf("Answer from %s is %v on %s\n", session.ApiKey, checked, strings.ReplaceAll(session.Text[base+i].Text, "\n", " "))
	}
	sessions[session.ApiKey] = log
}
