# GROTTO

## Getting started

Grotto is an example project how to drive UI from a Golang microservice. It is a simple questionnaire.

## Design decisions

Grotto is not that different than remote desktop solutions except the following.

- It runs the UI in the containers, so you can use simple Golang, Python, Unity, node.js, GPUs, etc. whatever your project requires.
- It does not use cookies. Customers will notice your brand right away by avoiding the distactions of the legally enforced 'Accept Cookie' buttons.
- It is experimental. We need to improve latency for example. It was not designed for animation but forms.
- It is still running in the browser, you do not need app store approvals, etc.
- It is Creative Commons Zero licensed. Take the code, cange it, use it, keep your changes secret, we do not care. Make sure that you review what you release. Reliability, any patent infringement, etc. is your responsibility.

## How to use

Download the sources
```
git clone https://gitlab.com/eper.io/grotto.git
```

Run the project
```
go run ./main.go
```

Check the site and log in with the activation key: `SQBABFELPLPPGPOOAHGJDRRLALFFLSDRSMERIGTAEBPFQIGCEJEGCSCAOOINQHRT`
Bookmark the administrator url.
```
http://127.0.0.1:7777/
```

You can give the survey link to your clients.
```
http://127.0.0.1:7777/survey.html
```

You can parse the plain text result format from the administrator interface logs.
```
http://127.0.0.1:7777/logs.md?apikey=FJOABRKMPAMBLKKNRFLFBCEQDFSDMKFPNPLJEAETEIKCBHQMCBSDCKGKKNBSQDKE

...

admin:FJOABR
In progress. Answer from CJQABEGHHEHESTHBKTQCIEBALHPBJMACGFGGCJIHGCFBBHGLQDQFHRDNIIIAIQHE is true on Do you work from a home office?  
Answer from CJQABEGHHEHESTHBKTQCIEBALHPBJMACGFGGCJIHGCFBBHGLQDQFHRDNIIIAIQHE is false on Have you spent on holiday travel in the last three months?
Answer from CJQABEGHHEHESTHBKTQCIEBALHPBJMACGFGGCJIHGCFBBHGLQDQFHRDNIIIAIQHE is true on Will you spend more on holiday travel in the next three months?
Answer from CJQABEGHHEHESTHBKTQCIEBALHPBJMACGFGGCJIHGCFBBHGLQDQFHRDNIIIAIQHE is false on      Have you spent on education in the last three months?
Answer from CJQABEGHHEHESTHBKTQCIEBALHPBJMACGFGGCJIHGCFBBHGLQDQFHRDNIIIAIQHE is false on Will you spend more on education in the next three months?
```

## License

This document is Licensed under Creative Commons CC0. To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this document to the public domain worldwide. This document is distributed without any warranty. You should have received a copy of the CC0 Public Domain Dedication along with this document. If not, see <https://creativecommons.org/publicdomain/zero/1.0/legalcode>.  